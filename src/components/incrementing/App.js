import React, { Component } from 'react';

export default class App extends Component {
    state = {
        number: 0
    }

    render() {
        return(
            <div>
                <Timer />
                <Counter name="Ali" />
                <Counter name="Alis" />
            </div>
        )
    }
}

class Timer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            date: new Date()
        }
    }

    componentDidMount() {
        this.TimerID = setInterval(() => this.tick(), 1000)
    }

    componentWillUnount() {
        clearInterval(this.TimerID)
    }

    tick() {
        this.setState({
            date: new Date()
        })
    }

    render() {
        return(
            <div>
                <h1>{this.state.date.toLocaleString()}</h1>
            </div>
        )
    }
}

class Counter extends Component {
    constructor(props) {
        super(props)
        this.addNumberHandler = this.addNumberHandler.bind(this)
        this.minusNumberHandler = this.minusNumberHandler.bind(this)
    }

    state = {
        number: 0
    }

    addNumberHandler = () => {
        this.setState({
            number: this.state.number + 1
        })
    }

    minusNumberHandler = () => {
        if (this.state.number < 1) {
            alert('tettotttttt!!!! min number is 0')
        } else {
            this.setState({
                number: this.state.number - 1
            })
        }
    }

    render() {
        return (
            <div>
                <h1>{this.props.name}</h1>
                <h1>{this.state.number}</h1>
                <button onClick={this.addNumberHandler}>plus</button>
                <button onClick={this.minusNumberHandler}>minus</button>
            </div>
        )
    }
}